package com.app.button_click;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    // 定义按键控件
    Button bt_Override, bt_Interface, bt_layout;
    Button bt_LongClick,bt_DownClick,bt_UpClick;

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // 绑定控件
        bt_Override = findViewById(R.id.bt_Override);
        bt_Interface = findViewById(R.id.bt_Interface);
        bt_layout = findViewById(R.id.bt_layout);
        bt_LongClick = findViewById(R.id.bt_LongClick);
        bt_DownClick = findViewById(R.id.bt_DownClick);
        bt_UpClick = findViewById(R.id.bt_UpClick);

        // 重写实现按键点击效果
        bt_Override.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toastShow("通过重写实现按键点击效果");
            }
        });

        // 接口实现入口（定义）
        bt_Interface.setOnClickListener(this);

        // 按键长按触发控件
        bt_LongClick.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                toastShow("长按了此控件");
                return false;   // 这个false是声明不使用单击
            }
        });

        // 按键按下时会触发
        bt_DownClick.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    toastShow("当按下时会触发这个Toast");
                }
                return false;
            }
        });

        // 按键抬起时会触发
        bt_UpClick.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    toastShow("当抬起时会触发这个Toast");
                }
                return false;
            }
        });

    }

    // 接口实现按键点击效果
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.bt_Interface:
                toastShow("通过接口实现按键点击效果");
                break;
        }
    }

    // 布局实现按键点击效果
    public void bt_layout(View view) {
        toastShow("通过布局实现按键点击效果");
    }

    /**气泡小弹窗方法
     * @param mes 显示文本
     */
    public void toastShow(String mes){
        Toast.makeText(this,mes,Toast.LENGTH_SHORT).show();
    }
}